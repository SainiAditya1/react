// import { useState } from "react";

import { useState } from "react";

// import "./App.css";

// function App() {
//   const [count, setCount] = useState(0);

//   return (
//     <>
//       <div>
//         <button onClick={() => setCount((count) => count + 1)}>
//           count is {count}
//         </button>
//       </div>
//     </>
//   );
// }

// export default App;

// jsx is a file , inside which u can write  both JS and xml.

// state management
// it will not work this way we have to use useState()
// let state = {
//   count: 0,
// };

function App() {
  const [count, setCount] = useState(0);

  function onClickHandler() {
    setCount(count + 1);
  }

  return <button onClick={onClickHandler}>COUNTER {count}</button>;
}
export default App;
